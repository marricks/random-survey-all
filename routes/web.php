<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


// Only pages are survey ones, so reroute to there
Route::redirect('/', 'survey/');


// Show all surveys newest to oldest & button to create new one
Route::get('survey/', 'SurveyController@index');

// View a single survey
Route::get('survey/{id}', 'SurveyController@view')->where('id', '[0-9]+');;
Route::post('survey/{id}', 'SurveyController@storeQuestion')->where('id', '[0-9]+');;

// Create a new survey
Route::get('survey/create/', 'SurveyController@create');
Route::post('survey/create/', 'SurveyController@store');

// Response to survey
Route::post('survey/response/', 'SurveyController@storeResponse');
