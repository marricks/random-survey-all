<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Survey;
use App\SurveyQuestion;
use App\SurveyQuestionResponse;


class SurveyController extends Controller
{
	/* PRIVATE */

	private function getQuestion()
	{
		$json = file_get_contents('http://jservice.io/api/random');
		$obj = json_decode($json);
		$question = $obj[0]->question;

		return $question . '?';
	}


	/* PUBLIC */

    public function __construct()
    {
        $this->surveys = Survey::orderBy('id', 'desc')->get();
    }

    /**
     * Show list of available surveys and button to create new one
     *
     * @return View
     */
    public function index()
    {
        $surveys = Survey::all();

        return view('survey.index', [
            'surveys' => $this->surveys
        ]);
    }

    /**
     * Return form to create survey
     *
     * @return View
     */
    public function create()
    {
		$question = $this->getQuestion();

        return view('survey.create', [
            'status' => '',
			'question' => $question,
            'surveys' => $this->surveys
        ]);
    }


    /**
     * Create a new survey
     * @return View
     */
    public function store(Request $request)
    {
        $request->validate([
            'survey_title' => 'required|max:255|min:5',
            'survey_text' => 'required|max:255|min:5|ends_with:?',
        ]);

        $title = $request->input('survey_title');
        $text = $request->input('survey_text');

        Survey::create(['title' => $title, 'question' => $text]);

        return back();
    }


    /**
     * View & answer existing survey
     *
     * @param int $id
     * @return View
     */
    public function view($id)
    {
        $survey = Survey::find($id);
        $questions = SurveyQuestion::all()->where('survey_id', $id);
        $total = 0;
        $counts = array();
        $percent = array();

        # get counts for each question
        foreach($questions as $question) {
            $questionId = $question['id'];
            $responses = SurveyQuestionResponse::all()->where('surveyQuestion_id', $questionId);
            $subtotal = count($responses);
            $total = $total + $subtotal;

            $counts[$questionId] = $subtotal;
        }

        # calculate percentage
        foreach($questions as $question) {
            $questionId = $question['id'];
            $subtotal = $counts[$questionId];

            if ($total != 0) {
                $decimal = $subtotal / $total;
            } else {
                $decimal = 0;
            }

            $percent[$questionId] = $decimal * 100;
        }

        return view('survey.view', [
            'self' => $survey,
            'questions' => $questions,
            'percent' => $percent,
            'surveys' => $this->surveys
        ]);
    }


    /**
     * Save question
     *
     * @param Request $request
     * @return View
     */
    public function storeQuestion(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255|min:5',
            'id' => 'required|numeric',
        ]);

        $id = $request->input('id');
        $title = $request->input('title');

        SurveyQuestion::create(['title' => $title, 'survey_id' => $id]);

        return back();
    }


    /**
     * Store survey question response
     *
     * @param Request $request
     * @return View
     */
    public function storeResponse(Request $request)
    {
        $request->validate([
            'question' => 'required|numeric'
        ]);

        $id = $request->input('question');
        $user = 'bob'; # TODO temporary

        SurveyQuestionResponse::create(['surveyQuestion_id' => $id, 'user' => $user]);

        return back();
    }
}
