<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'title',
        'question'
    ];

    // Title of survey
    public $title;

    // Survey question
    public $question;

    /**
     * Get all own survey questions
     */
    public function surveyQuestion()
    {
        return $this->hasMany('App\SurveyQuestion');
    }
}
