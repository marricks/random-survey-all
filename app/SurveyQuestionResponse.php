<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionResponse extends Model
{
    protected $fillable = [
        'user',
        'surveyQuestion_id'
    ];

    public $user = 'string';

    /**
     * Get survey that owns this question.
     */
    public function surveyQuestion()
    {
        return $this->belongsTo('App\SurveyQuestion');
    }
}
