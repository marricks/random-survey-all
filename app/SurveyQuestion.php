<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $fillable = [
        'title',
        'survey_id'
    ];

    // Question text
    public $title = 'string';

    /**
     * Get survey that owns this question.
     */
    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Get all owned question responses
     */
    public function surveyQuestion()
    {
        return $this->hasMany('App\SurveyQuestionResponse');
    }
}
