<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Surveys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('question');
            $table->timestamps();
        });

        Schema::create('survey_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->timestamps();

            $table->unsignedBigInteger('survey_id');
            $table->foreign('survey_id')->references('id')->on('surveys');
        });

        Schema::create('survey_question_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->timestamps();

            $table->unsignedBigInteger('surveyQuestion_id');
            $table->foreign('surveyQuestion_id')->references('id')->on('surveyQuestion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('surveyQuestions');
        Schema::dropIfExists('surveyQuestionResponses');
    }
}
