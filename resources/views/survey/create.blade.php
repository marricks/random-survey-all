@extends('app')

@section('main')

<h2>Make a new survey</h2>

<form method="POST" action="/survey/create/">
@csrf

<div class="form-group">
    <label for="survey_title">Title</label>
    <input type="text" class="form-control" id="survey_title" name="survey_title"
        value="{{ old('survey_title') }}">
</div>


<div class="form-group">
    <label for="survey_text">Question</label>
    <input type="text" class="form-control" id="survey_text" name="survey_text"
        value="{{ old('survey_text', $question) }}">
</div>

<button type="submit" class="btn btn-primary">Create</button>

</form>

<p>
    {{ $status }}
</p>


@endsection
