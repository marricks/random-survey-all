@extends('app')

@section('main')
<h2>{{ $self['title'] }}</h2>

<div class="mt-4">
    <h4>{{ $self['question'] }}</h4>

    <p>
    @foreach($questions as $question)

        <form method="POST" class="mb-2" action="/survey/response">
        @csrf
            <input type="hidden" value="{{ $question['id'] }}" id="question" name="question">

            <button type="submit" class="btn btn-sm btn-outline-secondary">{{ $question['title'] }}</button>

        </form>

        <div class="progress">
          <div class="progress-bar" role="progressbar" style="width: {{ $percent[$question['id']] }}%" aria-valuenow="{{ $percent[$question['id']] }}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <br />

    @endforeach
    </p>
</div>

<form method="POST" action="/survey/{{ $self['id'] }}">
@csrf
    <input type="hidden" value="{{ $self['id'] }}" id="id" name="id">

    <div class="form-row">
        <input type="text" class="form-control col-md-9" id="title" name="title"
            value="{{ old('title') }}">

        <button type="submit" class="btn btn-primary col-md-3 form-control">add question</button>
    </div>

</form>
@endsection
