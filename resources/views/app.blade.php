<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        .secondary-nav a,
        .secondary-nav > * {
            color: white;
        }
    </style>
</head>

<body>

    <header class="container-fluid main-header mt-3 mb-4">
        <div class="row">
            <h1 class="col-sm text-center mb-3">
                Survey app
            </h1>
        </div>
        <nav class="row navbar navbar-expand-sm navbar-dark bg-dark">
            <ul class="navbar-nav">
                <li class="nav-item"><a class="nav-link active" href="/">Home</a></li>
                <li class="nav-item"><a class="nav-link active" href="create">New survey</a></li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row main-wrapper border border-dark rounded-sm">

            <aside class="col-sm-4 p-2 bg-secondary border-right border-dark secondary-nav">
                <h3>Surveys</h3>

                <ul class="nav flex-column">
                @if ($surveys->isEmpty())
                    <li class="nav-item">
                        No surveys, go make one!
                    </li>
                @else
                    @foreach($surveys as $survey)
                    <li class="nav-item">
                        <a href="/survey/{{ $survey['id'] }}">{{ $survey['title'] }}</a>
                    </li>
                    @endforeach
                @endif
                </ul>

                <p>
                </p>
            </aside>

            <main class="col-sm-8 p-2">
                @section('main')
                    Choose a survey!
                @show


                @if ($errors->any())
                    <div class="mt-4 alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </main>

        </div>
    </div>

</body>

</html>
